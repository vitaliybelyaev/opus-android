package ru.belyaev.vitaliy.opusandroid

import android.Manifest.permission
import android.content.pm.PackageManager
import android.media.*
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_SHORT
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {
    private var sampleRate = 24000
    private var frameDuration = 20
    private var numberOfChannels = 1
    private var frameSize = sampleRate * frameDuration / 1000
    private var maxFrameSize = 1276 * 3
    private var audioRecorder: AudioRecord? = null
    private var audioPlayer: AudioTrack? = null
    private val opus = Opus()
    private val audioRecordingThread: WorkerThread = WorkerThread()
    private val playingThread: WorkerThread = WorkerThread()
    private var playBuffer: ShortArray? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initOpus()
        initializeAudioPlayer()

        audioRecordingThread.start()
        audioRecordingThread.prepareHandler()

        playingThread.start()
        playingThread.prepareHandler()
        playBuffer = ShortArray(frameSize)

        if (checkPermission()) {
            initAudioRecorder()
        }

        startButton.setOnClickListener {
            if (!checkPermission()) {
                requestPermission()
                return@setOnClickListener
            }
            startCall()
        }

        stopButton.setOnClickListener {
            isCalling = false
            callStatus.text = "Call ended"
            audioPlayer?.stop()
            audioRecorder?.stop()
        }
    }

    override fun onResume() {
        super.onResume()
        isCalling = false

        if (audioPlayer != null && audioPlayer!!.state == AudioTrack.PLAYSTATE_STOPPED) {
            audioPlayer!!.play()
        }
        if (audioRecorder != null && audioRecorder!!.state == AudioRecord.RECORDSTATE_STOPPED) {
            audioRecorder!!.startRecording()
        }
    }

    override fun onStop() {
        super.onStop()
        isCalling = false
        callStatus.text = "Call ended"
        if (audioPlayer != null) {
            audioPlayer!!.stop()
        }
        if (audioRecorder != null) {
            audioRecorder!!.stop()
        }
        opus.releaseEncoder()
        opus.releaseDecoder()
    }

    private fun initOpus() {
        opus.initEncoder(sampleRate, numberOfChannels, frameSize, maxFrameSize)
        opus.initDecoder(sampleRate, numberOfChannels, frameSize)
        Log.d(TAG, "Opus initialized")
    }

    private fun initializeAudioPlayer() {
        val buffSize = AudioTrack.getMinBufferSize(
            24000,
            AudioFormat.CHANNEL_OUT_MONO,
            AudioFormat.ENCODING_PCM_16BIT
        )
        audioPlayer = AudioTrack(
            AudioManager.STREAM_MUSIC, 24000, AudioFormat.CHANNEL_OUT_MONO,
            AudioFormat.ENCODING_PCM_16BIT, buffSize, AudioTrack.MODE_STREAM
        )
    }

    private fun initAudioRecorder() {
        audioRecorder = findAudioRecord()
        if (audioRecorder == null) {
            showMessage("Can not initialize audioRecorder")
        }
    }

    private fun findAudioRecord(): AudioRecord? {
        val channelConfig = AudioFormat.CHANNEL_IN_MONO.toShort()
        val audioFormat = AudioFormat.ENCODING_PCM_16BIT.toShort()
        try {
            Log.d(TAG, "Attempting rate $sampleRate Hz, audioFormat:$audioFormat")
            val minBufferSize =
                AudioRecord.getMinBufferSize(sampleRate, channelConfig.toInt(), audioFormat.toInt())
            val bufferSize = minBufferSize * 4
            val recorder = AudioRecord(
                MediaRecorder.AudioSource.MIC,
                sampleRate,
                channelConfig.toInt(),
                audioFormat.toInt(),
                bufferSize
            )
            if (recorder.state == AudioRecord.STATE_INITIALIZED) {
                Log.d(
                    TAG,
                    "Create AudioRecorder with rate $sampleRate Hz, audioFormat:$audioFormat, channelConfig:$channelConfig, bufferSize:$bufferSize"
                )
                return recorder
            }
        } catch (e: Exception) {
            Log.e(TAG, "Fail find AudioRecord with sample rate:$sampleRate", e)
        }
        return null
    }

    private fun startCall() {
        isCalling = true
        audioPlayer!!.play()
        callStatus.text = "In call..."
        audioRecordingThread.postTask(Runnable { startAudioStream() })
    }

    private fun startAudioStream() {
        audioRecorder!!.startRecording()
        var encoded: Int
        val inBuf = ShortArray(frameSize)
        val outBuf = ByteArray(maxFrameSize)
        Log.d(
            TAG,
            "Create inBuf size:" + inBuf.size + ", outBuf size:" + outBuf.size
        )
        while (isCalling) {
            audioRecorder!!.read(inBuf, 0, inBuf.size)
            Log.d(TAG, "inBuf size:${inBuf.size}")

            encoded = opus.encode(inBuf, outBuf)
            val sendBuf = ByteArray(encoded)
            System.arraycopy(outBuf, 0, sendBuf, 0, encoded)

            Log.d(TAG, "Encoded size:$encoded, Encoded sendBuf:${getOutBufferData(sendBuf)}")
            playWithDelay(sendBuf, 0)
        }
    }

    private fun playWithDelay(outBuf: ByteArray, delayInMillis: Long) {
        playingThread.postDelayTask(delayInMillis, Runnable {
            val numOfDecodedSamples: Int = opus.decode(outBuf, playBuffer)
            Log.d(TAG, "numOfDecodedSamples $numOfDecodedSamples")
            audioPlayer!!.write(playBuffer!!, 0, playBuffer!!.size)
        })
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(permission.RECORD_AUDIO),
            AUDIO_REQUEST_CODE
        )
    }

    private fun checkPermission(): Boolean {
        val result: Int =
            ContextCompat.checkSelfPermission(applicationContext, permission.RECORD_AUDIO)
        return result == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            AUDIO_REQUEST_CODE -> if (grantResults.isNotEmpty()) {
                val audioAccepted =
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
                if (audioAccepted) {
                    initAudioRecorder()
                    Handler(Looper.getMainLooper())
                        .postDelayed({ startCall() }, 3000)
                } else {
                    showMessage("Permission Denied, can't start call")
                }
            }
        }
    }

    private fun getOutBufferData(outBuf: ByteArray): List<Byte> {
        val result: MutableList<Byte> = ArrayList()
        for (i in outBuf.indices) {
            result.add(outBuf[i])
        }
        return result
    }

    private fun showMessage(text: String) {
        Snackbar.make(callStatus, text, LENGTH_SHORT).show()
    }

    companion object {
        const val TAG = "OpusAndroid"
        const val AUDIO_REQUEST_CODE = 101
        var isCalling = false
    }
}

