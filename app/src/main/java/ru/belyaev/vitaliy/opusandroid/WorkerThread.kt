package ru.belyaev.vitaliy.opusandroid

import android.os.Handler
import android.os.HandlerThread

internal class WorkerThread : HandlerThread(TAG) {
    private var mWorkerHandler: Handler? = null
    fun prepareHandler() {
        mWorkerHandler = Handler(looper)
    }

    fun postTask(task: Runnable) {
        mWorkerHandler?.post(task)
    }

    fun postDelayTask(delayInMillis: Long, task: Runnable) {
        if (delayInMillis == 0L) {
            mWorkerHandler?.post(task)
            return
        }
        mWorkerHandler?.postDelayed(task, delayInMillis)
    }

    companion object {
        const val TAG = "WorkerThread"
    }
}