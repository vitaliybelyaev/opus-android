package ru.belyaev.vitaliy.opusandroid

class Opus {
    init {
        System.loadLibrary("jniopus")
    }

    external fun initEncoder(
        samplingRate: Int,
        numberOfChannels: Int,
        frameSize: Int,
        maxFrameSize: Int
    ): Boolean

    external fun encodeBytes(`in`: ShortArray?, out: ByteArray?): Int

    external fun releaseEncoder(): Boolean

    external fun initDecoder(
        samplingRate: Int,
        numberOfChannels: Int,
        frameSize: Int
    ): Boolean

    external fun decodeBytes(`in`: ByteArray?, out: ShortArray?): Int

    external fun releaseDecoder(): Boolean


    fun encode(`in`: ShortArray?, out: ByteArray?): Int {
        return encodeBytes(`in`, out)
    }

    fun decode(encodedBuffer: ByteArray?, buffer: ShortArray?): Int {
        return decodeBytes(encodedBuffer, buffer)
    }
}